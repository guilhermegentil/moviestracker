package app.controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.models.movie.Movie;
import app.models.user.User;
import app.utils.ServletUtilities;

/**
 * 
 * @author guilhermeamaral
 *
 */
@WebServlet(name="MovieServlet", urlPatterns={"/addToFavorites"})
public class MovieController extends HttpServlet{
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		String redirectTo = "";
		
		User loggedUser = (User)ServletUtilities.getDataFromSession("logged_user", request);
		
		if (request.getParameter("action").equals("add")){
			
			Object dataFromSession = ServletUtilities.getDataFromSession( "moviesList", request);
			
			@SuppressWarnings("unchecked")
			ArrayList<Movie> moviesList = (dataFromSession instanceof ArrayList<?> ? (ArrayList<Movie>) dataFromSession : new ArrayList<Movie>());
			
			for(Movie m : moviesList){
				if (m.getId() == Integer.parseInt(request.getParameter("id"))){
					m.save();
					loggedUser.AddToFavorites(m);
					redirectTo = "/WEB-INF/pages/profile.jsp";
				}
			}
			
		}else if(request.getParameter("action").equals("remove")){
			loggedUser.RemoveFromFavorites(Integer.parseInt(request.getParameter("id")));
			redirectTo = "/WEB-INF/pages/profile.jsp";
		}
		request.getRequestDispatcher(redirectTo).forward(request, response);
	}
	
}
