package app.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import app.models.movie.Movie;
import app.models.user.User;

/**
 * 
 * @author guilhermeamaral
 *
 */
public class ServletUtilities {

	/**
	 * 
	 * @param cookies
	 * @param cookieName
	 * @param defaultVal
	 * @return
	 */
	public static String getCookieValue(Cookie[] cookies, String cookieName, String defaultVal) {
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				if (cookieName.equals(cookie.getName()))
					return (cookie.getValue());
			}
		}
		return (defaultVal);
	}

	/**
	 * 
	 * @param cookies
	 * @param cookieName
	 * @return
	 */
	public static Cookie getCookie(Cookie[] cookies, String cookieName) {
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				if (cookieName.equals(cookie.getName()))
					return (cookie);
			}
		}
		return (null);
	}
	
	/**
	 * 
	 * @param attrName
	 * @param attrValue
	 * @param request
	 */
	public static void storeDataInSession(String attrName, Object attrValue, HttpServletRequest request) {
		HttpSession mySession = request.getSession(true);
		
		if (attrValue instanceof String)
			mySession.setAttribute(attrName, (String)attrValue);
		else if (attrValue instanceof User)
			mySession.setAttribute(attrName, (User)attrValue);
		else if (attrValue instanceof Movie)
			mySession.setAttribute(attrName, (Movie)attrValue);
		else
			mySession.setAttribute(attrName, attrValue);		
		
	}
	
	/**
	 * 
	 * @param attrName
	 * @param request
	 * @return
	 */
	public static Object getDataFromSession(String attrName, HttpServletRequest request){
		HttpSession mySession = request.getSession(true);
		return mySession.getAttribute(attrName);
	}
	
	/**
	 * 
	 * @param attrName
	 * @param request
	 */
	public static void removeDataFromSession(String attrName, HttpServletRequest request) {
		HttpSession mySession = request.getSession(true);
		mySession.removeAttribute(attrName);
	}
	
	/**
	 * 
	 * @param attrName
	 * @param attrValue
	 * @param response
	 */
	public static void storeDataInCookie(String attrName, String attrValue, HttpServletResponse response) {
		storeDataInCookie(attrName, attrValue, response, 3600);
	}

	/**
	 * 
	 * @param attrName
	 * @param attrValue
	 * @param response
	 * @param cookieTTL
	 */
	public static void storeDataInCookie(String attrName, String attrValue, HttpServletResponse response, Integer cookieTTL) {
		Cookie cookie = new Cookie(attrName,attrValue);
		cookie.setMaxAge(cookieTTL);
		response.addCookie(cookie);
	}

}
