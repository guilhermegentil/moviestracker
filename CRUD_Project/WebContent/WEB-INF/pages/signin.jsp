<!DOCTYPE html>
<head>
<title>Series Tracker</title>

<!-- Bootstrap core CSS -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="dist/js/jquery.js"></script>
<script src="dist/js/bootstrap.min.js"></script>

</head>

<body>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#welcome_modal').modal('show');
		});
	</script>

	<div class="modal fade" id="welcome_modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Sign In</h4>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<form accept-charset="UTF-8" action="access?action=signin"
									class="form-signin" id="new_user" method="post">
									<div style="margin: 0; padding: 0; display: inline">
										<input name="utf8" type="hidden" value="&#x2713;" /><input
											name="authenticity_token" type="hidden"
											value="e5N2uxVJCISL58pDFpeAEFTrFOiRo6qz+A0uojADO04=" />
									</div>
									<div class="container"></div>

									<table align="center">
										<tr>
											<td><label for="user_name">Email</label> <input
												class="form-control" id="user[email]" name="user[email]"
												type="text" /></td>
											<td></td>
											<td><label for="user_surname">Password</label> <input
												class="form-control" id="user[password]"
												name="user[password]" type="password" /></td>

										</tr>
									</table>

									<p>
										<input class="btn btn-success" name="commit" type="submit"
											value="Sign in" /> <a class="btn btn-danger"
											href="index.jsp">Back</a>
									</p>
									<p>
										<!-- 
										<input id="remember_me" type="checkbox" name="remember_me"
											value="1" checked="1" /> <label for="remember_me">Remember
											me</label>
									 -->
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

</body>
</html>