package app.models.movie;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import app.models.base.DAOBase;

/**
 * 
 * @author guilhermeamaral
 *
 */
public class MovieDAO extends DAOBase {

	/**
	 * 
	 * @param movie
	 */
	public void create(Movie movie) {
		try{
			String sql = "INSERT INTO movies(id, name, overview, poster_url, score)" + " VALUES (?, ?, ?, ?, ?)";
			
			PreparedStatement ps = getConnection().prepareStatement(sql);

			ps.setInt(1, movie.getId());
			ps.setString(2, movie.getName());
			ps.setString(3, movie.getOverview());
			ps.setString(4, movie.getPoster_url());
			ps.setFloat(5, movie.getScore());
			
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param movieId
	 * @return
	 */
	public Movie retrieve(Integer movieId) {
		Movie existingMovie = null;

		try {
			String sql = "SELECT id, name, overview, poster_url, score FROM movies WHERE id=? LIMIT 1";
			PreparedStatement ps = getConnection().prepareStatement(sql);

			ps.setInt(1, movieId);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Integer id = rs.getInt("id");
				String name = rs.getString("name");
				String overview = rs.getString("overview");
				String poster_url = rs.getString("poster_url");
				Float score = rs.getFloat("score");
				
				existingMovie = new Movie(id, name, overview, poster_url, score);
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return existingMovie;
	}

}
