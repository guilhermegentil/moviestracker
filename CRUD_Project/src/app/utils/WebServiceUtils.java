package app.utils;

import argo.jdom.*;
import argo.saj.InvalidSyntaxException;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import app.models.movie.Movie;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * 
 * @author guilhermeamaral
 *
 */
public class WebServiceUtils {

	private static String moviedbParamQuery = "query";
	private static String moviedbParamKey = "api_key";
	private static String moviedbKey = "2701947a8f4b238d3f1ef3b98f51922a";
	private static String moviedbUrl = "http://api.themoviedb.org/3/";

	private static URI getBaseURI() {
		return UriBuilder.fromUri(moviedbUrl).build();
	}

	/**
	 * 
	 * @param movieName
	 * @return
	 */
	public static ArrayList<Movie> searchMovieByName(String movieName) {
		String responseJSON;
		ArrayList<Integer> movieIds;
		ArrayList<Movie> movies = new ArrayList<Movie>();

		responseJSON = httpGet(movieName, "search/movie");
		movieIds = parseMovieJSONMovieID(responseJSON);

		for (Integer movieID : movieIds) {
			movies.add(getMovieByID(movieID));
		}

		return movies;
	}

	/**
	 * 
	 * @param movieID
	 * @return
	 */
	private static Movie getMovieByID(Integer movieID) {
		String responseJSON;

		responseJSON = httpGet(null, "movie/" + movieID);
		Movie movie = parseMovieJSON(responseJSON);

		return movie;
	}

	/**
	 * 
	 * @param jsonString
	 * @return
	 */
	private static Movie parseMovieJSON(String jsonString) {
		Movie movie = null;
		try {
			JsonNode result = new JdomParser().parse(jsonString).getNode();

			Integer movieID = Integer.parseInt(result.getNumberValue("id"));
			String movieName = result.getNullableStringValue("original_title");
			String movieOverview = result.getNullableStringValue("overview");
			String moviePosterURL = result.getNullableStringValue("poster_path");
			Float movieScore = Float.parseFloat(result.getNullableNumberValue("vote_average"));
			
			movie = new Movie(movieID, movieName, movieOverview, moviePosterURL, movieScore);

		} catch (InvalidSyntaxException e) {
			e.printStackTrace();
		}
		
		return movie;
	}

	/**
	 * 
	 * @param jsonString
	 * @return
	 */
	private static ArrayList<Integer> parseMovieJSONMovieID(String jsonString) {
		ArrayList<Integer> movieIdList = new ArrayList<Integer>();
		try {
			List<JsonNode> results = new JdomParser().parse(jsonString)
					.getArrayNode("results");
			for (JsonNode node : results) {
				movieIdList.add(Integer.parseInt(node.getNumberValue("id")));
			}
		} catch (InvalidSyntaxException e) {
			e.printStackTrace();
		}

		return movieIdList;
	}

	/**
	 * 
	 * @param query
	 * @param path
	 * @return
	 */
	private static String httpGet(String query, String path) {
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());

		String result = "";

		MultivaluedMapImpl params = new MultivaluedMapImpl();
		params.add(moviedbParamKey, moviedbKey);

		if (query != null)
			params.add(moviedbParamQuery, query);

		try {
			result = service.path(path).queryParams(params)
					.accept(MediaType.APPLICATION_JSON_TYPE).get(String.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
