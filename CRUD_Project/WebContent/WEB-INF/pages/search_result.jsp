<%@ page import="java.util.ArrayList"%>
<%@ page import="app.models.movie.Movie"%>
<%@ page import="app.models.user.User"%>

<jsp:include page="shared/_header.jsp" />

<%
User loggedUser = (User) session.getAttribute("logged_user");
ArrayList<Movie> moviesList = (ArrayList<Movie>) session.getAttribute( "moviesList");
ArrayList<Movie> favoritesList = loggedUser.getMovies();
%>

<div class="container">
	<h3>Search Result.</h3>
	<table class="table table-striped table-bordered table-condensed">
		<th>Poster</th>
		<th>Name</th>
		<th>Overview</th>
		<th></th>

		<% 
	for (int i = 0; i < moviesList.size(); i++){
		out.write("<tr>");
		out.write("<td width='155px'>");
		out.write("<img src='http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w154"+moviesList.get(i).getPoster_url()+"'/>");
		out.write("</td>");
		out.write("<td width='155px'>" + moviesList.get(i).getName() + "</td>");
		out.write("<td>" + moviesList.get(i).getOverview() + "</td>");
		
		if (favoritesList.contains(moviesList.get(i))){
			out.write("<td> <a class='btn btn-danger'" + "href='addToFavorites?action=remove&id="+moviesList.get(i).getId()+"'>ADD</a></td>");
		}else{
			out.write("<td> <a class='btn btn-info'" + "href='addToFavorites?action=add&id="+moviesList.get(i).getId()+"'>ADD</a></td>");
		}
		
		out.write("</tr>");
	} 
	%>
	</table>
</div>

<jsp:include page="shared/_footer.jsp" />
