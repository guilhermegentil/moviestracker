package app.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.utils.ServletUtilities;

/**
 * 
 * @author guilhermeamaral
 *
 */
@WebServlet(name="RootServlet", urlPatterns={"/index.jsp"})
public class RootController extends HttpServlet{
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		String redirectTo = "";
		
		if (ServletUtilities.getDataFromSession("logged_user", request) != null){ // Means that a user is already logged in
			redirectTo = "/WEB-INF/pages/profile.jsp";
		}
		else{
			redirectTo = "/WEB-INF/pages/welcome.jsp";
		}
		request.getRequestDispatcher(redirectTo).forward(request, response);
	}

}
