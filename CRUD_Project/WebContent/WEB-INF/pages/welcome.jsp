<!DOCTYPE html>
<head>
<title>Series Tracker</title>

<!-- Bootstrap core CSS -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="dist/js/jquery.js"></script>
<script src="dist/js/bootstrap.min.js"></script>

</head>

<body>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#welcome_modal').modal('show');
		});		
	</script>

	<div class="modal fade" id="welcome_modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Welcome. Are you a new user?</h4>
				</div>
				<div class="modal-body" align="center">
					<table>
						<tr>
							<td><a class="btn btn-lg btn-primary"
								href="welcome?action=new" role="button">New User</a></td>
							<td><a class="btn btn-lg btn-success"
								href="welcome?action=existing" role="button">Existing User</a></td>
						</tr>
					</table>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

</body>
</html>