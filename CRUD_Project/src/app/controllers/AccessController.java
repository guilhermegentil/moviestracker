package app.controllers;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.models.user.User;
import app.utils.ServletUtilities;

/**
 * 
 * @author guilhermeamaral
 *
 */
@WebServlet(name = "AccessServlet", urlPatterns = { "/access" })
public class AccessController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String redirectTo = "";

		if (action == null || action.equals("")) {
			redirectTo = "#";
		} else if (action.equals("logout")) {
			ServletUtilities.removeDataFromSession("sessionID", request);
			ServletUtilities.removeDataFromSession("logged_user", request);

			redirectTo = "/index.jsp";
		}

		request.getRequestDispatcher(redirectTo).forward(request, response);
	}

	/**
	 * 
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String redirectTo = "";

		if (action.equals("signup")) {
			String userFirstName = request.getParameter("user[firstname]");
			String userLastName = request.getParameter("user[lastname]");
			String userEmail = request.getParameter("user[email]");
			String userBirthday = request.getParameter("user[birthday]");
			String userGender = request.getParameter("user[gender]");
			String userPassword = hash(request.getParameter("user[password]"),
					"MD5");

			User existingUser = User.FindUserByEmail(userEmail);

			if (existingUser != null) {
				
				request.setAttribute("show_flash", true);
				ArrayList<String> errorMessages = new ArrayList<String>();
				
				errorMessages.add("Email already used.");
				request.setAttribute("validation_erros", errorMessages);

				redirectTo = "/WEB-INF/pages/signup.jsp";
				
			} else {

				User newUser = new User(userFirstName, userLastName, userEmail, userBirthday, userGender, userPassword);
				
				redirectTo = "/WEB-INF/pages/profile.jsp";

				if (newUser.isValid()) {
					if (newUser.save() != null) {
						newUser = User.FindUser(newUser.getEmail(), newUser.getPasswordDigest());
						ServletUtilities.storeDataInSession("logged_user", newUser, request);
						redirectTo = "/WEB-INF/pages/profile.jsp";
					}
				} else {
					request.setAttribute("show_flash", true);
					request.setAttribute("validation_erros",
							newUser.getValidationErrors());

					redirectTo = "/WEB-INF/pages/signup.jsp";
				}
			}

			request.getRequestDispatcher(redirectTo).forward(request, response);

		} else if (action.equals("signin")) {
			String userEmail = request.getParameter("user[email]");
			String userPassword = hash(request.getParameter("user[password]"),
					"MD5");

			User existingUser = User.FindUser(userEmail, userPassword);

			if (existingUser != null) {
				ServletUtilities.storeDataInSession("logged_user",
						existingUser, request);
				redirectTo = "/WEB-INF/pages/profile.jsp";
			}
			request.getRequestDispatcher(redirectTo).forward(request, response);
		}
	}

	/**
	 * 
	 * @param input
	 * @param algorithm
	 * @return
	 */
	private String hash(String input, String algorithm) {
		String hash = null;

		if (input == null || input.equals(""))
			return null;

		try {
			// Create MessageDigest object for MD5
			MessageDigest digest = MessageDigest.getInstance(algorithm);

			// Update input string in message digest
			digest.update(input.getBytes(), 0, input.length());

			// Converts message digest value in base 16 (hex)
			hash = new BigInteger(1, digest.digest()).toString(16);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hash;
	}

}
