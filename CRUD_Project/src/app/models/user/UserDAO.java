package app.models.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import app.models.base.DAOBase;
import app.models.movie.Movie;

/**
 * 
 * @author guilhermeamaral
 *
 */
public class UserDAO extends DAOBase {

	/**
	 * 
	 * @param user
	 */
	public void create(User user) {
		try {
			String sql = "INSERT INTO users(firstname, lastname, email, birthday, gender, password)"
					+ " VALUES (?, ?, ?, ?, ?, ?)";

			PreparedStatement ps = getConnection().prepareStatement(sql);

			ps.setString(1, user.getFirstName());
			ps.setString(2, user.getLastName());
			ps.setString(3, user.getEmail());
			ps.setDate(4, java.sql.Date.valueOf((user.getBirthday())));
			ps.setString(5, user.getGender());
			ps.setString(6, user.getPasswordDigest());

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param userEmail
	 * @param passwordDigest
	 * @return
	 */
	public User retrieve(String userEmail, String passwordDigest) {
		User existingUser = null;

		try {
			String sql = "SELECT id, firstname, lastname, email, birthday, gender FROM users WHERE email=? and password=? LIMIT 1";
			PreparedStatement ps = getConnection().prepareStatement(sql);

			ps.setString(1, userEmail);
			ps.setString(2, passwordDigest);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Integer id = rs.getInt("id");
				String firstname = rs.getString("firstname");
				String lastname = rs.getString("lastname");
				String email = rs.getString("email");
				String birthday = rs.getDate("birthday").toString();
				String gender = rs.getString("gender");

				existingUser = new User(id, firstname, lastname, email,
						birthday, gender);
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return existingUser;
	}
	
	/**
	 * 
	 * @param userEmail
	 * @return
	 */
	public User retrieve(String userEmail) {
		User existingUser = null;

		try {
			String sql = "SELECT id, firstname, lastname, email, birthday, gender FROM users WHERE email=? LIMIT 1";
			PreparedStatement ps = getConnection().prepareStatement(sql);

			ps.setString(1, userEmail);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Integer id = rs.getInt("id");
				String firstname = rs.getString("firstname");
				String lastname = rs.getString("lastname");
				String email = rs.getString("email");
				String birthday = rs.getDate("birthday").toString();
				String gender = rs.getString("gender");

				existingUser = new User(id, firstname, lastname, email,
						birthday, gender);
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return existingUser;
	}

	/**
	 * 
	 * @param user_id
	 * @param movie_id
	 */
	public void addToFavorites(Integer user_id, Integer movie_id) {
		try {
			String sql = "INSERT INTO favorites(user_id, movie_id) VALUES (?, ?)";

			PreparedStatement ps = getConnection().prepareStatement(sql);

			ps.setInt(1, user_id);
			ps.setInt(2, movie_id);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public ArrayList<Movie> getFavorites(Integer user_id) {
		ArrayList<Movie> result = new ArrayList<Movie>();
		try {
			String sql = "SELECT m.id, m.name, m.overview, m.poster_url, m.score FROM movies m join favorites f on m.id = f.movie_id and f.user_id = ?";
			PreparedStatement ps = getConnection().prepareStatement(sql);

			ps.setInt(1, user_id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Integer id = rs.getInt("id");
				String name = rs.getString("name");
				String overview = rs.getString("overview");
				String poster_url = rs.getString("poster_url");
				Float score = rs.getFloat("score");

				result.add(new Movie(id, name, overview, poster_url, score));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 
	 * @param user_id
	 * @param movie_id
	 */
	public void removeFromFavorites(Integer user_id, Integer movie_id) {
		try {
			String sql = "DELETE FROM favorites WHERE user_id=? AND movie_id=?";

			PreparedStatement ps = getConnection().prepareStatement(sql);

			ps.setInt(1, user_id);
			ps.setInt(2, movie_id);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}		

}
