<!DOCTYPE html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

<title>My Movies</title>

<!-- Bootstrap core CSS -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="dist/css/custom/navbar.css" rel="stylesheet">

<style type="text/css">
body {
	padding-top: 80px;
}
</style>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<div class="container">

		<!-- Static navbar -->
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="welcome">My Movies</a>
			</div>
			<form accept-charset="UTF-8" action="search"
				class="navbar-form navbar-left" method="get">
				<div style="margin: 0; padding: 0; display: inline">
					<input name="utf8" type="hidden" value="&#x2713;" />
				</div>
				<div class="form-group">
					<input class="form-control" id="query" name="query"
						placeholder="Search New Movie..." type="text" />
				</div>
				<input class="btn btn-default" name="commit" type="submit"
					value="Search" />
			</form>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Settings <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="welcome">Profile</a></li>
							<li class="divider"></li>
							<li><a href="access?action=logout">Logout</a></li>
						</ul></li>
				</ul>
			</div>

			<!--/.nav-collapse -->
		</div>