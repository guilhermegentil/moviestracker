package app.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.utils.ServletUtilities;

/**
 * 
 * @author guilhermeamaral
 *
 */
@WebServlet(name = "WelcomeServlet", urlPatterns = { "/welcome" })
public class WelcomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");
		String redirectTo = "";
		
		if (ServletUtilities.getDataFromSession("logged_user", request) != null) {
			redirectTo = "/WEB-INF/pages/profile.jsp";
		} else {
			if (action == null || action.equals("")) {
				redirectTo = "/index.jsp";
			} else if (action.equals("existing")) {
				redirectTo = "/WEB-INF/pages/signin.jsp";
			} else if (action.equals("new")) {
				redirectTo = "/WEB-INF/pages/signup.jsp";
			}
		}
		request.getRequestDispatcher(redirectTo).forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
