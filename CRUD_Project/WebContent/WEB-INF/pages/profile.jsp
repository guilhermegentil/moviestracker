<%@ page import="java.util.ArrayList"%>
<%@ page import="app.models.movie.Movie"%>
<%@ page import="app.models.user.User"%>
<jsp:include page="shared/_header.jsp" />

<%
User loggedUser = (User) session.getAttribute("logged_user");
ArrayList<Movie> favoritesList = loggedUser.getMovies();
%>

<!-- Main component for a primary marketing message or call to action -->
<div class="container">
	<div>
		<h2>
			Welcome,
			<%= loggedUser.getFirstName() +" "+ loggedUser.getLastName()%></h2>
	</div>
</div>

<div class="container">
	<h3>My favorite movies.</h3>
	<table class="table table-striped table-bordered table-condensed">
		<th>Poster</th>
		<th>Name</th>
		<th>Overview</th>
		<th></th>

		<% 
		for (Movie m : loggedUser.getMovies()){
			out.write("<tr>");
			out.write("<td width='155px'>");
			out.write("<img src='http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w154"+m.getPoster_url()+"'/>");
			out.write("</td>");
			out.write("<td width='155px'>" + m.getName() + "</td>");
			out.write("<td>" + m.getOverview() + "</td>");
			out.write("<td> <a class='btn btn-danger'" + "href='addToFavorites?action=remove&id="+m.getId()+"'>REMOVE</a></td>");
			out.write("</tr>");
		} 
		%>
	</table>
</div>

<jsp:include page="shared/_footer.jsp" />