package app.models.movie;

import java.io.Serializable;

/**
 * 
 * @author guilhermeamaral
 *
 */
public class Movie implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String name;
	private String overview;
	private String poster_url;
	private Float score;

	private MovieDAO dao;
	
	/**
	 * 
	 */
	public Movie(){	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param overview
	 * @param poster_url
	 * @param score
	 */
	public Movie(Integer id, String name, String overview, String poster_url, Float score) {
		this.id = id;
		this.name = name;
		this.overview = overview;
		this.poster_url = poster_url;
		this.score = score;

		this.dao = new MovieDAO();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOverview() {
		return overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public String getPoster_url() {
		return poster_url;
	}

	public void setPoster_url(String poster_url) {
		this.poster_url = poster_url;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	/**
	 * 
	 */
	public void save() {
		if (this.dao.retrieve(this.id) == null){
			this.dao.create(this);
		}
	}

}
