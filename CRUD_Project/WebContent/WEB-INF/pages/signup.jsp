<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html>
<head>
<title>Series Tracker</title>

<!-- Bootstrap core CSS -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="dist/js/jquery.js"></script>
<script src="dist/js/bootstrap.min.js"></script>

</head>

<body>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#welcome_modal').modal('show');
		});
	</script>

	<div class="modal fade" id="welcome_modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Sign Up</h4>
				</div>
				<div class="modal-body">
					<!-- Main component for a primary marketing message or call to action -->
					<div class="container">

						<%
							if (request.getAttribute("show_flash") != null) {
								out.write("<div id='error_explanation'><div class='alert alert-error'>The form contains errors.</div><ul>");
								for (String errorMsg : (ArrayList<String>) request.getAttribute("validation_erros")) {
									out.write("<li>*" + errorMsg + "</li>");
								}
								out.write("</ul></div>");
							}
						%>

						<div class="row">
							<div class="col-md-4">
								<form accept-charset="UTF-8" action="access?action=signup"
									class="form-signin" id="new_user" method="post">
									<div style="margin: 0; padding: 0; display: inline">
										<input name="utf8" type="hidden" value="&#x2713;" /><input
											name="authenticity_token" type="hidden"
											value="e5N2uxVJCISL58pDFpeAEFTrFOiRo6qz+A0uojADO04=" />
									</div>
									<div class="container"></div>
									<p>
									<table>
										<tr>
											<td><label for="user_name">Name</label> <input
												class="form-control" id="user[firstname]"
												name="user[firstname]" type="text" /></td>
											<td></td>
											<td><label for="user_surname">Surname</label> <input
												class="form-control" id="user_lastname"
												name="user[lastname]" type="text" /></td>
										</tr>
									</table>
									</p>
									<p>
										<label for="user_email">Email</label> <input
											class="form-control" id="user_email" name="user[email]"
											type="text" />
									</p>
									<p>
										<label for="user_birthday">Birthday</label> <input
											class="form-control" id="user_birthday" name="user[birthday]"
											type="date" />
									</p>
									<p>
										<span> <input id="user_gender_male" name="user[gender]"
											type="radio" value="MALE" /> Male
										</span> <span> <input id="user_gender_female"
											name="user[gender]" type="radio" value="FEMALE" /> Female
										</span>
									</p>
									<p>
										<label for="user_password">Password</label> <input
											class="form-control" id="user_password" name="user[password]"
											type="password" />
									</p>
									<p>
										<input class="btn btn-large btn-primary" name="commit"
											type="submit" value="Create my account" /> <a
											class="btn btn-danger" href="index.jsp">Back</a>
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

</body>
</html>







