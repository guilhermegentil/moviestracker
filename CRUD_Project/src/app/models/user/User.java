package app.models.user;

import java.io.Serializable;
import java.util.ArrayList;

import app.models.movie.Movie;

/**
 * 
 * @author guilhermeamaral
 *
 */
public class User implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String firstName;
	private String lastName;
	private String email;
	private String birthday;
	private String gender;
	private String passwordDigest;
	
	private ArrayList<Movie> movies;
	
	private UserDAO dao;
	
	public User(){}
	
	/**
	 * 
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param birthday
	 * @param gender
	 * @param passwordDigest
	 */
	public User(Integer id, String firstName, String lastName, String email, String birthday, String gender, String passwordDigest){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthday = birthday;
		this.gender = gender;
		this.passwordDigest = passwordDigest;
		
		this.dao = new UserDAO();
	}
	
	/**
	 * 
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param birthday
	 * @param gender
	 */
	public User(Integer id, String firstName, String lastName, String email, String birthday, String gender){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthday = birthday;
		this.gender = gender;
		
		this.dao = new UserDAO();
	}
	
	/**
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param birthday
	 * @param gender
	 * @param passwordDigest
	 */
	public User(String firstName, String lastName, String email, String birthday, String gender, String passwordDigest){
		this.id = null;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthday = birthday;
		this.gender = gender;
		this.passwordDigest = passwordDigest;
		
		this.dao = new UserDAO();
	}
	
	/**
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param birthday
	 * @param gender
	 */
	public User(String firstName, String lastName, String email, String birthday, String gender){
		this.id = null;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthday = birthday;
		this.gender = gender;
		
		this.dao = new UserDAO();
	}
	
	/**
	 * 
	 * @param email
	 * @param passwordDigest
	 * @return
	 */
	public static User FindUser(String email, String passwordDigest){
		UserDAO dao = new UserDAO();
		
		return dao.retrieve(email, passwordDigest);
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getId() {
		return id;
	}

	public String getPasswordDigest() {
		return passwordDigest;
	}

	public ArrayList<Movie> getMovies() {
		return this.dao.getFavorites(this.id);
	}

	public void setMovies(ArrayList<Movie> movies) {
		this.movies = movies;
	}

	public void AddToFavorites(Movie m) {
		this.dao.addToFavorites(this.id, m.getId());
	}

	public void RemoveFromFavorites(Integer movieId) {
		this.dao.removeFromFavorites(this.id, movieId);
	}
	
	/**
	 * 
	 * @return
	 */
	public User save(){
		dao.create(this);
		return dao.retrieve(this.email, this.passwordDigest);
	}

	/**
	 * 
	 * @return
	 */
	public boolean isValid() {
		if (this.getValidationErrors().size() > 0)
			return false;
		return true;
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<String> getValidationErrors() {
		ArrayList<String> validationErros = new ArrayList<String>();
		
		if(this.firstName == null || this.firstName.equals(""))
			validationErros.add("First name is mandatory");
		if(this.lastName == null || this.lastName.equals(""))
			validationErros.add("Surname is mandatory");
		if(this.email == null || this.email.equals(""))
			validationErros.add("Email is mandatory");
		if(this.birthday == null || this.birthday.equals(""))
			validationErros.add("Birthday is mandatory");
		if(this.passwordDigest == null ||this.passwordDigest.equals(""))
			validationErros.add("Password is mandatory");
		
		return validationErros;
	}

	/**
	 * 
	 * @param userEmail
	 * @return
	 */
	public static User FindUserByEmail(String userEmail) {
		UserDAO dao = new UserDAO();
		
		return dao.retrieve(userEmail);
	}
	
}
