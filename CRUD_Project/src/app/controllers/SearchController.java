package app.controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.models.movie.Movie;
import app.utils.ServletUtilities;
import app.utils.WebServiceUtils;

/**
 * 
 * @author guilhermeamaral
 *
 */
@WebServlet(name="SearchServlet", urlPatterns={"/search"})
public class SearchController extends HttpServlet{
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String searchQuery = request.getParameter("query");
		ArrayList<Movie> result =  WebServiceUtils.searchMovieByName(searchQuery);
		
		ServletUtilities.storeDataInSession("moviesList", result, request);
		
		request.getRequestDispatcher("/WEB-INF/pages/search_result.jsp").forward(request, response);
	}
	
}
