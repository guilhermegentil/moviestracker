package app.models.base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 
 * @author guilhermeamaral
 *
 */
public class DAOBase {

	private static Connection connection = null;

	private static final String driver = "org.postgresql.Driver";
	private static final String url = "jdbc:postgresql://localhost:5432/webapp_dev";
	private static final String user = "series_tracker";
	private static final String password = "series_tracker";

	/**
	 * 
	 * @return
	 */
	protected Connection getConnection() {
		if (connection != null)
			return connection;
		else {
			try {
				Class.forName(driver);
				connection = DriverManager.getConnection(url, user, password);
			} catch (ClassNotFoundException cnfe) {
				System.out.println(cnfe);
			} catch (SQLException sqe) {
				System.out.println(sqe);
			}
			return connection;
		}
	}

	/**
	 * 
	 */
	protected void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
